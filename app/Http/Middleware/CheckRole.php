<?php

namespace App\Http\Middleware;

use Closure;

class CheckRole
{
	/**
	 * Handle the incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @param  int  $roleId
	 * @return mixed
	 */
	public function handle($request, Closure $next, $roleId)
	{
		if (!$request->user()->hasRole($roleId)) {
			abort(401, 'This action is unauthorized.');
		}

		return $next($request);
	}

}