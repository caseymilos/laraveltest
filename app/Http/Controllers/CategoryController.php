<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\DB;

class CategoryController extends BaseController
{
	use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

	public function index() {
		$categories = Category::all();
		return view('categories.categories', ['categories' => $categories]);
	}

	public function singleCategory($id) {
		$category = Category::where('id', $id)->firstOrFail();
		return view('categories.category', ['category' => $category]);
	}

	public function getAdd() {
		$categories = Category::all();
		return view('categories.addCategory', ['categories' => $categories]);
	}

	public function postAdd(Request $request){
		$category = $request->input('category');

		 DB::table('categories')->insertGetId(
			['caption' => $category]
		);
		return redirect('/#categories');
	}

	public function getEdit($id) {

		$category = Category::where('id', '=', $id)->firstOrFail();
		return view('categories.editCategory', ['category' => $category]);
	}

	public function postEdit($id, Request $request){
		$category = $request->input('name');

		DB::table('categories')->where('id', $id)->update(
			['caption' => $category]
		);

		DB::table('category_product')->where('product_id', $id)->delete();


		return redirect('/#categories');
	}

	public function getDelete($id) {
		DB::table('categories')->where('id', $id)->delete();

		return redirect('/categories');
	}

}
