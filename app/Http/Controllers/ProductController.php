<?php

namespace App\Http\Controllers;

use App\Category;
use App\Product;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\DB;

class ProductController extends BaseController
{
	use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

	public function index() {
		$products = Product::with('categories')->get();
		return view('products.products', ['products' => $products]);
	}

	public function singleProduct($id) {
		$product = Product::where('id', $id)->firstOrFail();
		return view('products.product', ['product' => $product]);
	}

	public function getAdd() {
		$categories = Category::all();

		return view('products.addProduct', ['categories' => $categories]);
	}

	public function postAdd(Request $request){
		$name = $request->input('name');
		$description = $request->input('description');
		$price = $request->input('price');
		$categories = $request->input('categoryId');

		$picture = $request->file('picture');
		$imageName = "";
		if (!empty($picture)) {
			$imageName = time() . '.' . $picture->getClientOriginalExtension();
			$destinationPath = public_path('/images');
			$picture->move($destinationPath, $imageName);
		}
		$productId = DB::table('products')->insertGetId(
			['name' => $name, 'description' => $description, 'price' => $price, 'picture' => $imageName]
		);

		if($categories) {
			foreach ($categories as $categoryId) {
				DB::table('category_product')->insert(
					['category_id' => $categoryId, 'product_id' => $productId]
				);
			}
		}

		return redirect('/#products');
	}

	public function getEdit($id) {
		$product = Product::where('id', '=', $id)->firstOrFail();
		$categories = Category::all();

		return view('products.editProduct', ['product' => $product, 'categories' => $categories]);
	}

	public function postEdit($id, Request $request){
		$name = $request->input('name');
		$description = $request->input('description');
		$price = $request->input('price');
		$categories = $request->input('categoryId');

		$picture = $request->file('picture');
		$imageName = "";
		if (!empty($picture)) {
			$imageName = time() . '.' . $picture->getClientOriginalExtension();
			$destinationPath = public_path('/images');
			$picture->move($destinationPath, $imageName);
		}

		DB::table('products')->where('id', $id)->update(
			['name' => $name, 'description' => $description, 'price' => $price, 'picture' => $imageName]
		);

		DB::table('category_product')->where('product_id', $id)->delete();

		if($categories) {
			foreach ($categories as $categoryId) {
				DB::table('category_product')->insert(
					['category_id' => $categoryId, 'product_id' => $id]
				);
			}
		}

		return redirect('/#products');
	}

	public function getDelete($id) {
		DB::table('products')->where('id', $id)->delete();

		return redirect('/#products');
	}

}