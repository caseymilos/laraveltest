@extends('layouts.app')

@section('content')
    <div class="menu-wrapper">
        <div class="valign-wrapper">
            <div class="menu">
                <p>Menu</p>
                <ul id="mainMenu">
                    <li><a id="openProducts" href="#products" data-href="/products"><i class="fa fa-shopping-cart"></i>
                            Products <span class="badge">1</span></a></li>
                    <li><a id="openCategories" href="#categories" data-href="/categories"><i class="fa fa-tags"></i>
                            Categories <span class="badge">2</span></a></li>
                </ul>
            </div>
        </div>
    </div>
    <div id="mainContent" class="content-wrapper">

    </div>
@endsection

@section('footer_scripts')
    <script type="text/javascript">
		$(document).on("click", '#mainMenu li a', function (e) {

			var route = $(this).data('href');
			var target = $(this).attr('href');

			if (target === '#products') {
				$('.add-button').addClass('hide');
				$('#addProductAction').removeClass('hide');
			} else if(target === '#categories') {
				$('.add-button').addClass('hide');
				$('#addCategoryAction').removeClass('hide');
			} else {
				$('.add-button').addClass('hide');
			}

			$.ajax({
				url: route,
				method: "GET",
				data: {},
				success: function (data) {
					console.log(data);
					$('.menu-wrapper').addClass('opened');
					$('.valign-wrapper').addClass('activated');
					$('#mainContent').html(data);
				}
			});
		});

		$(document).on("click", '.product-name', function (e) {
			e.preventDefault();

			var route = $(this).attr('href');
			$('#modal1').modal('open');
			$.ajax({
				url: route,
				method: "GET",
				data: {},
				success: function (data) {
					console.log(data);
					$('#modalContent').html(data);
				}
			});
		});

		$(document).on("click", '.open-edit-product', function (e) {
			e.preventDefault();

			var route = $(this).attr('href');
			$('#modal1').modal('open');
			$.ajax({
				url: route,
				method: "GET",
				data: {},
				success: function (data) {
					console.log(data);
					$('#modalContent').html(data);
					$('select').material_select();
				}
			});
		});

		$(document).on("click", '.open-edit-category', function (e) {
			e.preventDefault();

			var route = $(this).attr('href');
			$('#modal1').modal('open');
			$.ajax({
				url: route,
				method: "GET",
				data: {},
				success: function (data) {
					console.log(data);
					$('#modalContent').html(data);
					$('select').material_select();
				}
			});
		});

		$(document).on("click", '.delete-product', function (e) {
			e.preventDefault();
			var route = $(this).attr('href');
			var id = $(this).data('id');

			$.confirm({
				title: 'Are you sure?',
				content: 'You are about to delete this product. Are you sure?',
				buttons: {
					confirm: function () {
						$.ajax({
							url: route,
							method: "GET",
							data: {},
							success: function (data) {
								$('#product' + id).remove();
								Materialize.toast('Deleted', 1000);
							}
						});
					},
					cancel: function () {}
				}
			});
		});

		$(document).on("click", '.delete-category', function (e) {
			e.preventDefault();
			var route = $(this).attr('href');
			var id = $(this).data('id');

			$.confirm({
				title: 'Are you sure?',
				content: 'You are about to delete this category. Are you sure?',
				buttons: {
					confirm: function () {
						$.ajax({
							url: route,
							method: "GET",
							data: {},
							success: function (data) {
								$('#category' + id).remove();
								Materialize.toast('Deleted', 1000);
							}
						});
					},
					cancel: function () {}
				}
			});
		});

		if(window.location.href.indexOf("#products") > -1) {
			$('#openProducts').click();
			$('.add-button').addClass('hide');
			$('#addProductAction').removeClass('hide');
		} else {
			$('#addProductAction').addClass('hide');
		}
		if(window.location.href.indexOf("#categories") > -1) {
			$('#openCategories').click();
			$('.add-button').addClass('hide');
			$('#addCategoryAction').removeClass('hide');
		} else {
			$('#addCategoryAction').addClass('hide');
		}
    </script>
@endsection
