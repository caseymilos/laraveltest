@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col s8 offset-s2">
                <div class="card">
                    <div class="card-title">Add product</div>
                    <div class="card-content">
                        <form method="post" enctype="multipart/form-data">
                            <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">

                            <div class="row">
                                <div class="input-field col s12">
                                    <input id="name" type="text" name="name" class="validate">
                                    <label for="name">Name</label>
                                </div>
                            </div>

                            <div class="row">
                                <div class="input-field col s12">
                                    <textarea id="description" name="description" class="materialize-textarea"></textarea>
                                    <label for="description">Description</label>
                                </div>
                            </div>

                            <div class="row">
                                <div class="input-field col s12">
                                    <input id="name" type="number" step="0.1" min="0" name="price" class="validate">
                                    <label for="name">Price</label>
                                </div>
                            </div>

                            <div class="row">
                                <div class="file-field col s12 input-field">
                                    <div class="btn">
                                        <span>File</span>
                                        <input type="file" name="picture">
                                    </div>
                                    <div class="file-path-wrapper">
                                        <input class="file-path validate" type="text">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="input-field col s12">
                                    <select name="categoryId[]" multiple>
                                        @if ($categories)
                                            @foreach($categories as $category)
                                                <option value="{{ $category->id }}">{{ $category->Caption }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                    <label>Categories</label>
                                </div>
                            </div>

                            <div class="row">
                                <div class="input-field col s12">
                                    <button class="btn">Save</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('footer_scripts')
    <script type="text/javascript">
		$(document).ready(function() {
			$('select').material_select();
		});
    </script>
@endsection