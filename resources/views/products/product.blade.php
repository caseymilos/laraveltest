@php $categories = []; @endphp
@if($product->categories)
    @foreach($product->categories as $category)


        @php $categories[] = $category->Caption; @endphp
    @endforeach
@endif
<div class="row">
    <div class="col s6">
        <img src="/images/{{$product->Picture}}" style="width: 100%">
    </div>
    <div class="col s6">
        <h4 class="product-title">{{$product->Name}}</h4>

        <p>{{$product->Description}}</p>

        <p class="price">{{$product->Price}}<span>$</span></p>
            @foreach($product->categories as $category)
                <span class="chip"><i class="fa fa-tags"></i> {{$category->Caption}}</span>
            @endforeach
    </div>
</div>
