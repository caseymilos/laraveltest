<form method="post" id="editForm" action="/edit-product/{{ $product->id }}" enctype="multipart/form-data">
    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">

    <div class="row">
        <div class="input-field col s12">
            <input id="name" type="text" value="{{$product->Name}}" name="name" placeholder="Product name">
            <label for="name">Name</label>
        </div>
    </div>

    <div class="row">
        <div class="input-field col s12">
            <textarea id="description" name="description" class="materialize-textarea">{{$product->Description}}</textarea>
            <label for="description">Description</label>
        </div>
    </div>

    <div class="row">
        <div class="input-field col s12">
            <input id="name" type="number" step="0.1" min="0" name="price">
            <label for="name">Price</label>
        </div>
    </div>

    <div class="row">
        <div class="file-field col s12 input-field">
            <div class="btn">
                <span>File</span>
                <input type="file" name="picture">
            </div>
            <div class="file-path-wrapper">
                <input class="file-path validate" type="text">
            </div>
        </div>
    </div>

    <div class="row">
        <div class="input-field col s12">
            <select name="categoryId[]" multiple>
                @if ($categories)
                    @foreach($categories as $category)
                        <option value="{{ $category->id }}">{{ $category->Caption }}</option>
                    @endforeach
                @endif
            </select>
            <label>Categories</label>
        </div>
    </div>

    <div class="row">
        <div class="input-field col s12">
            <button id="editProductSubmit" class="btn">Save</button>
        </div>
    </div>
</form>