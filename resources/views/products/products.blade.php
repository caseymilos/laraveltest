@php $categories = []; @endphp

<div class="row">
    @foreach($products as $product)
        @if($product->categories)
            @foreach($product->categories as $category)
                @php $categories[] = $category->Caption; @endphp
            @endforeach
        @endif
        <div id="product{{ $product->id }}" class="col s4">
            <div class="card">
                <a class="product-name modal-trigger" data-target="modal1"
                   href="/product/{{ $product->id }}">
                    <div class="card-image">
                        <img src="/images/{{$product->Picture}}">
                    </div>
                </a>
                <div class="card-title">
                    <a class="product-name modal-trigger" data-target="modal1"
                       href="/product/{{ $product->id }}">{{$product->Name}}</a>
                </div>
                <div class="card-content">
                    <p>
                        {{str_limit($product->Description, 100)}}
                    </p>
                    <p class="price">{{$product->Price}}<span>$</span></p>
                </div>

                <div class="card-action">
                    <a href="/edit-product/{{ $product->id }}" class="btn btn-secondary open-edit-product">Edit</a> |
                    <a href="/delete-product/{{ $product->id }}" class="black-text delete-product" data-id="{{ $product->id }}"><i class="fa fa-trash"></i> Delete</a>
                </div>
            </div>
        </div>
    @endforeach
</div>