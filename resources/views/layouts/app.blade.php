<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'Laravel') }}</title>

        <!-- Styles -->
        <!-- Compiled and minified CSS -->
        <link rel="stylesheet"
              href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css">
        <link rel="stylesheet"
              href="https://maxcdn.icons8.com/fonts/line-awesome/1.1/css/line-awesome-font-awesome.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">
        <link href="{{ asset('css/style.css') }}" rel="stylesheet">

    </head>
    <body>
        <div id="app">
            <div class="navbar-fixed">
                <nav>
                    <div class="nav-wrapper">
                        <a href="{{ url('/') }}" class="brand-logo">{{ config('app.name', 'Laravel') }}</a>
                        <ul id="nav-mobile" class="right hide-on-med-and-down">
                            @guest
                                <li><a href="{{ route('login') }}">Login</a></li>
                                <li><a href="{{ route('register') }}">Register</a></li>
                            @else

                                <li id="addProductAction" class="add-button hide">
                                    <a href="/add-product" class="btn">
                                        Add product
                                    </a>
                                </li>

                                <li id="addCategoryAction" class="add-button hide">
                                    <a href="/add-category" class="btn">
                                        Add category
                                    </a>
                                </li>

                                <li>
                                    <a href="#">
                                        {{ Auth::user()->name }}
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        Logout
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                          style="display: none;">
                                        @csrf
                                    </form>
                                </li>
                            @endguest
                        </ul>
                    </div>
                </nav>
            </div>

            <main class="py-4">
                @yield('content')
            </main>
        </div>

        <!-- Modal Structure -->
        <div id="modal1" class="modal">
            <div id="modalContent" class="modal-content">
                <h4>Modal Header</h4>
                <p>A bunch of text</p>
            </div>
        </div>

        <div class="preloader-top">
            <div class="action-section"></div>
        </div>
        <div class="preloader-bottom">
            <div class="action-section"></div>
        </div>

        <!-- Scripts -->
        <script src="{{ asset('js/jquery.min.js') }}"></script>
        <!-- Compiled and minified JavaScript -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script>
        <script type="text/javascript">
			$(document).ready(function () {

				$(".preloader-top").addClass('open');
				$(".preloader-bottom").addClass('open');
				$(".main-section").addClass('loaded');
				$("#app").addClass('loaded');
				$('select').material_select();
				$('.modal').modal();
			});
        </script>
        @yield('footer_scripts')
    </body>
</html>
