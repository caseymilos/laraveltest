<form method="post" action="/edit-category/{{ $category->id }}" enctype="multipart/form-data">
    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">

    <div class="row">
        <div class="input-field col s12">
            <input id="name" type="text" value="{{$category->Caption}}" name="name" class="validate">
            <label for="name">Name</label>
        </div>
    </div>

    <div class="row">
        <div class="input-field col s12">
            <button class="btn">Save</button>
        </div>
    </div>
</form>