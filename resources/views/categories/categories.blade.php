<div class="card">
    <div class="card-title">Categories</div>
    <div class="card-content">
        <table class="table">
            <thead>
                <tr>
                    <th>Name</th>
                    <th class="right-align">
                    </th>

                </tr>
            </thead>
            <tbody>
                @foreach($categories as $category)
                    <tr id="category{{ $category->id }}">
                        <td><a href="/category/{{ $category->id }}">{{$category->Caption}}</a></td>

                        <td class="right-align"><a href="/edit-category/{{ $category->id }}"
                                                   class="btn btn-secondary open-edit-category">Edit</a> |
                            <a href="/delete-category/{{ $category->id }}" class="delete-category black-text" data-id="{{ $category->id }}">
                                <i class="fa fa-trash"></i> Delete
                            </a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>