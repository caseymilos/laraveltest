@extends('layouts.app')

@section('content')
   <div class="container">
       <div class="row">
           <div class="col s8 offset-s2">
               <div class="card">
                   <div class="card-title">Add category</div>
                   <div class="card-content">
                       <form method="post" enctype="multipart/form-data">
                           <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">

                           <div class="row">
                               <div class="input-field col s12">
                                   <input id="name" type="text" name="category" class="validate">
                                   <label for="name">Name</label>
                               </div>
                           </div>

                           <div class="row">
                               <div class="input-field col s12">
                                   <button class="btn">Save</button>
                               </div>
                           </div>
                       </form>
                   </div>
               </div>
           </div>
       </div>
   </div>
@endsection