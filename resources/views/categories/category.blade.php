<?php
?>

<div class="container">
    <table class="table">
        <thead>
            <tr>
                <td>Name</td>

            </tr>
        </thead>
        <tbody>
            <tr>
                @if($category)
                        <th>{{$category->Caption}}</th>
                        <th><a href="/edit-category/{{$category->id}}" class="btn">Edit</a> | <a
                                    href="/delete-category/{{$category->id}}" class="btn">Delete</a></th>
                @endif
            </tr>
        </tbody>
    </table>


</div>
