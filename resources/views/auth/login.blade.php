@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col l6 s12 offset-l3">
                <div class="card">
                    <div class="card-title">Login</div>

                    <div class="card-content">
                        <form method="POST" action="{{ route('login') }}">
                            @csrf

                            <div class="row">

                                <div class="input-field col s12">
                                    <input id="email" type="email"
                                           class="{{ $errors->has('email') ? ' is-invalid' : '' }}"
                                           name="email" value="{{ old('email') }}" required autofocus>
                                    <label for="email">E-Mail Address</label>
                                </div>
                                <div class="col s12">
                                    @if ($errors->has('email'))
                                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="row">

                                <div class="input-field col s12">
                                    <input id="password" type="password"
                                           class="{{ $errors->has('password') ? ' is-invalid' : '' }}"
                                           name="password" required>
                                    <label for="password">Password</label>
                                </div>
                                <div class="col s12">
                                    @if ($errors->has('password'))
                                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="row">
                                <div class="col s12">
                                    <p>
                                        <input type="checkbox" class="filled-in" id="remember-me" checked="checked"
                                               name="remember" {{ old('remember') ? 'checked' : '' }} />
                                        <label for="remember-me">Remember Me</label>
                                    </p>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col s12">
                                    <button type="submit" class="btn btn-primary">
                                        Login
                                    </button>
                                    <br>
                                    <br>
                                    <a href="{{ route('password.request') }}">
                                        Forgot Your Password?
                                    </a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
