<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');

// Products
Route::get('/products', 'ProductController@index');
Route::get('/add-product', 'ProductController@getAdd')->middleware('role:1');
Route::post('/add-product', 'ProductController@postAdd')->middleware('role:1');
Route::get('/edit-product/{id}', 'ProductController@getEdit')->middleware('role:2');
Route::post('/edit-product/{id}', 'ProductController@postEdit')->middleware('role:2');
Route::get('/delete-product/{id}', 'ProductController@getDelete')->middleware('role:3');


Route::get('/product/{id}', 'ProductController@singleProduct');


// Categories
Route::get('/categories', 'CategoryController@index')->middleware('role:1');
Route::get('/add-category', 'CategoryController@getAdd')->middleware('role:1');
Route::post('/add-category', 'CategoryController@postAdd')->middleware('role:1');
Route::get('/edit-category/{id}', 'CategoryController@getEdit')->middleware('role:1');
Route::post('/edit-category/{id}', 'CategoryController@postEdit')->middleware('role:1');
Route::get('/delete-category/{id}', 'CategoryController@getDelete')->middleware('role:1');

Route::get('/category/{id}', 'CategoryController@singleCategory')->middleware('role:1');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
